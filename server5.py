from http.server import HTTPServer, BaseHTTPRequestHandler

HOST_ADDRESS = "212.128.254.65"
HOST_PORT = 20090

class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        if (self.path == "/hola"):
           f = open("."+self.path, "r")
           msg = f.read()
        else:
            msg = "Fichero no encontrado"
        self.wfile.write("GET request for {} ".format(self.path).encode('utf-8'))
        self.wfile.write(msg.encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself

        self.set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """ follows example shown on docs.python.org """
    server_address = (HOST_ADDRESS, HOST_PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == '__main__':
    run(handler_class=RequestHandler)