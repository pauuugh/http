from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse

HOST_ADDRESS = "127.0.0.1" #este codigo si luego pongo localhost; si no pongo mi ip en los dos lados
HOST_PORT = 20090
#duda, donde miro mis servidores arrancados?
formhtml = """
<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms</h2>

<form action="/action_page">
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname" value="John"><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname" value="Doe"><br><br>
  <input type="submit" value="Submit">
</form>
</body>
</html>"""

responsehtml = """
<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms</h2>

<p> Hola {nombre}!</p>
<p> La ruta es {ruta}!</p>

<form action="/action_page">
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname" value="{nombre}"><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname" value="{apellido}"><br><br>
  <input type="submit" value="Submit">
</form>

</body>
</html>"""
#p ej en el examen nos pueden pedir que editemos el formulario para que nos de la hora --> buscar como hacer que py me de la hora
class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        if (self.path =="/" or self.path == "/index.html"): #pido la url antes del submit y como no hay parametros ni query pongo ==
            self.wfile.write(formhtml.encode('utf-8'))
        elif (self.path.startswith("/action_page")):   #path es todo lo que va despues del puerto en la url y startswish es empieza con; aqui uso starwish porque como el path ha crecido mucho solo me interesa ver si es cierto que empieza con {action_page}
            query = urlparse(self.path).query  #cogge el path y dame solo el query, query es despues de la ?
            query_components = dict(qc.split("=") for qc in query.split("&"))
            fname = query_components["fname"]
            lname = query_components["lname"]
            print(fname, lname)
            self.wfile.write(responsehtml.format(nombre=fname, apellido=lname, ruta=self.path).encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself

        self.set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        self.wfile.write("POST data is {}".format(post_data ).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """ follows example shown on docs.python.org """
    server_address = (HOST_ADDRESS, HOST_PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == '__main__':
    run(handler_class=RequestHandler)


