#!/usr/bin/python
from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 21080

class miServidor(BaseHTTPRequestHandler):

        # Este metodo gestiona las peticiones GET de HTTP
        def do_GET(self):
                self.send_response(200)
                self.send_header('Content-type','text/html')
                self.end_headers()
                # Nos retorna la respuesta
                with open("index.html", 'r') as f:
                        content=f.readlines()
                        print(content)
                        for line in content:
                                self.wfile.write(line.encode("utf-8"))
                return
                #self.wfile.write("<!DOCTYPE html>".encode("utf-8"))
                #self.wfile.write("<html>".encode("utf-8"))
                #self.wfile.write("<head>".encode("utf-8"))
                #self.wfile.write("<title>Page Title</title>".encode("utf-8"))
                #self.wfile.write("</head>".encode("utf-8"))
                #self.wfile.write("<body>".encode("utf-8"))
                #self.wfile.write("<h1>This is a Heading</h1>".encode("utf-8"))
                #self.wfile.write("<p>This is a paragraph.</p>".encode("utf-8"))
                #self.wfile.write("</body>".encode("utf-8"))
                #self.wfile.write("</html>".encode("utf-8"))

try:
        server = HTTPServer(('', PORT_NUMBER), miServidor)
        print('Started httpserver on port ' , PORT_NUMBER)

        #Wait forever for incoming http requests
        server.serve_forever()

except KeyboardInterrupt:
        print('Control-C received, shutting down the web server')
        server.socket.close()
