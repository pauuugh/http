from http.server import HTTPServer, BaseHTTPRequestHandler

PORT_NUMBER = 21080

class miServidor(BaseHTTPRequestHandler):

        # Este metodo gestiona las peticiones GET de HTTP
        #yo soy como este pero cambio algo; cojo la clase de py por defecto y la modifico a mi gusto
        def do_GET(self):
                self.send_response(200)
                self.send_header('Content-type','text/html; charset=utf-8') #aqui escribo el texto
                self.end_headers()
                #estas 3 lineas anteriores son la cabecera donde prepara la respuesta del protocolo HTTP
                # El servidor nos manda la respuesta
                self.wfile.write("Hola boba !".encode("utf-8"))  #esto es el texto, me lo manda como un mensaje
                return

try:
        server = HTTPServer(('', PORT_NUMBER), miServidor) #se crea el servidor
        print('Started httpserver on port ' , PORT_NUMBER)

        #Wait forever for incoming http requests
        server.serve_forever() #se arranca el servidor

except KeyboardInterrupt:
        print('Control-C received, shutting down the web server')
        server.socket.close()

