from http.server import HTTPServer, BaseHTTPRequestHandler
from urllib.parse import urlparse

HOST_ADDRESS = "212.128.254.65"
HOST_PORT = 20090

formhtml = """
<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms</h2>

<form action="/action_page">
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname" value="John"><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname" value="Doe"><br><br>
  <input type="submit" value="Submit">
</form>

<p>If you click the "Submit" button, the form-data will be sent to a page called "/action_page".</p>

</body>
</html>"""

responsehtml = """
<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms</h2>

<p> Hola Amigos!</p>

</body>
</html>"""

class RequestHandler(BaseHTTPRequestHandler):
    def set_response(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.set_response()
        if (self.path =="/" or self.path == "/index.html"):
            self.wfile.write(formhtml.encode('utf-8'))
        elif (self.path.startswith("/action_page")):
            query = urlparse(self.path).query
            query_components = dict(qc.split("=") for qc in query.split("&"))
            fname = query_components["fname"]
            lname = query_components["lname"]
            print(fname, lname)
            self.wfile.write(responsehtml.encode('utf-8'))

    def do_POST(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        post_data = self.rfile.read(content_length) # <--- Gets the data itself

        self.set_response()
        self.wfile.write("POST request for {}".format(self.path).encode('utf-8'))
        self.wfile.write("POST data is {}".format(post_data ).encode('utf-8'))

def run(server_class=HTTPServer, handler_class=BaseHTTPRequestHandler):
    """ follows example shown on docs.python.org """
    server_address = (HOST_ADDRESS, HOST_PORT)
    httpd = server_class(server_address, handler_class)
    httpd.serve_forever()

if __name__ == '__main__':
    run(handler_class=RequestHandler)
